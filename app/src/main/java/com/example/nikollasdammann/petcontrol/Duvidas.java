package com.example.nikollasdammann.petcontrol;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Duvidas extends AppCompatActivity {

    EditText editTextNome, editTextEmail, editTextDuvida;
    Button buttonEnviar;


    public boolean isOnline() {
        try {
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnectedOrConnecting();
        }
        catch(Exception ex){
            Toast.makeText(getApplicationContext(), "Erro ao verificar se estava online! (" + ex.getMessage() + ")", Toast.LENGTH_SHORT).show();
            return false;
}
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_duvidas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

       editTextNome = (EditText) findViewById(R.id.editTextNome);
       editTextEmail= (EditText) findViewById(R.id.editTextEmail);

       buttonEnviar = (Button) findViewById(R.id.buttonEnviar);
       buttonEnviar.setOnClickListener(new Button.OnClickListener() {
           @Override
           public void onClick(View v) {
           }

    private void enviarEmail() {

        final String nome = editTextNome.getText().toString();
        final String email = editTextEmail.getText().toString();
        final String subject = "assunto do email";
        final String body = "testepet " + nome;

        if(!isOnline()) {
            Toast.makeText(getApplicationContext(), "Não estava online para enviar e-mail!", Toast.LENGTH_SHORT).show();
            System.exit(0);
        }

        new Thread(new Runnable(){
            @Override
            public void run() {
                Mail m = new Mail();

                String[] toArr = {email};
                m.setTo(toArr);

                //m.setFrom("seunome@seuemail.com.br"); //caso queira enviar em nome de outro
                m.setSubject(subject);
                m.setBody(body);

                try {
                    //m.addAttachment("pathDoAnexo");//anexo opcional
                    m.send();
                }
                catch(RuntimeException rex){ }//erro ignorado
                catch(Exception e) {
                    e.printStackTrace();
                    System.exit(0);
                }

                Toast.makeText(getApplicationContext(), "Email enviado, responderemos em 24 horas!", Toast.LENGTH_SHORT).show();
            }
        }).start();

    }
       });
    }
    }
