package com.example.nikollasdammann.petcontrol;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.Vector;

public class Youtube extends AppCompatActivity {

    RecyclerView recyclerView;

    Vector<YoutubeVideo> youtubeVideos = new Vector<YoutubeVideo>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager( new LinearLayoutManager(this));

        youtubeVideos.add( new YoutubeVideo ("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/-iVZp6YCl_A\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        youtubeVideos.add( new YoutubeVideo ("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/-iVZp6YCl_A\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        youtubeVideos.add( new YoutubeVideo ("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/-iVZp6YCl_A\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        youtubeVideos.add( new YoutubeVideo ("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/-iVZp6YCl_A\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));
        youtubeVideos.add( new YoutubeVideo ("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/-iVZp6YCl_A\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>"));

        VideoAdapter videoAdapter = new VideoAdapter(youtubeVideos);

        recyclerView.setAdapter(videoAdapter);


    }
}
