package com.example.nikollasdammann.petcontrol;

public class YoutubeVideos {

    String videoUrl;

    public YoutubeVideos() {

    }

    public YoutubeVideos(String videoURL)
    {this.videoUrl = videoUrl;}

    public String getVideoUrl()
    {return videoUrl;
    }

    public void setVideoUrl (String videoUrl)
    {this.videoUrl = videoUrl;
    }

}
