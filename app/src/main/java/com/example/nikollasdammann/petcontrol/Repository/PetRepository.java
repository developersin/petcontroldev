package com.example.nikollasdammann.petcontrol.Repository;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import com.example.nikollasdammann.petcontrol.Uteis.DatabaseUtil;
import com.example.nikollasdammann.petcontrol.Model.PetModel;

/**
 * Created by cicero.machado on 18/03/2016.
 */
public class PetRepository {

    DatabaseUtil databaseUtil;

    /***
     * CONSTRUTOR
     * @param context
     */
    public PetRepository(Context context){

        databaseUtil =  new DatabaseUtil(context);

    }

    /***
     * SALVA UM NOVO REGISTRO NA BASE DE DADOS
     * @param petModel
     */
    public void Salvar(PetModel petModel){

        ContentValues contentValues =  new ContentValues();
        /*MONTANDO OS PARAMETROS PARA SEREM SALVOS*/
        contentValues.put("ds_nome",       petModel.getNome());
        contentValues.put("ds_endereco",   petModel.getEndereco());
        contentValues.put("fl_sexo",       petModel.getSexo());
        contentValues.put("dt_nascimento", petModel.getDataNascimento());
        contentValues.put("fl_estadoCivil",petModel.getEstadoCivil());
        contentValues.put("fl_ativo",      petModel.getRegistroAtivo());

        /*EXECUTANDO INSERT DE UM NOVO REGISTRO*/
        databaseUtil.GetConexaoDataBase().insert("tb_pet",null,contentValues);

    }

    /***
     * ATUALIZA UM REGISTRO JÁ EXISTENTE NA BASE
     * @param petModel
     */
    public void Atualizar(PetModel petModel){

        ContentValues contentValues =  new ContentValues();

        /*MONTA OS PARAMENTROS PARA REALIZAR UPDATE NOS CAMPOS*/
        contentValues.put("ds_nome",       petModel.getNome());
        contentValues.put("ds_endereco",   petModel.getEndereco());
        contentValues.put("fl_sexo",       petModel.getSexo());
        contentValues.put("dt_nascimento", petModel.getDataNascimento());
        contentValues.put("fl_estadoCivil",petModel.getEstadoCivil());
        contentValues.put("fl_ativo",      petModel.getRegistroAtivo());

        /*REALIZANDO UPDATE PELA CHAVE DA TABELA*/
        databaseUtil.GetConexaoDataBase().update("tb_pet", contentValues, "id_pet = ?", new String[]{Integer.toString(petModel.getCodigo())});
    }

    /***
     * EXCLUI UM REGISTRO PELO CÓDIGO
     * @param codigo
     * @return
     */
    public Integer Excluir(int codigo){

        //EXCLUINDO  REGISTRO E RETORNANDO O NÚMERO DE LINHAS AFETADAS
        return databaseUtil.GetConexaoDataBase().delete("tb_pet","id_pet = ?", new String[]{Integer.toString(codigo)});
    }

    /***
     * CONSULTA UM PET CADASTRADA PELO CÓDIGO
     * @param codigo
     * @return
     */
    public PetModel GetPet(int codigo){


        Cursor cursor =  databaseUtil.GetConexaoDataBase().rawQuery("SELECT * FROM tb_pet WHERE id_pet= "+ codigo,null);

        cursor.moveToFirst();

        ///CRIANDO UM NOVO PET
        PetModel petModel =  new PetModel();

        //ADICIONANDO OS DADOS DO PET
        petModel.setCodigo(cursor.getInt(cursor.getColumnIndex("id_pet")));
        petModel.setNome(cursor.getString(cursor.getColumnIndex("ds_nome")));
        petModel.setEndereco(cursor.getString(cursor.getColumnIndex("ds_endereco")));
        petModel.setSexo(cursor.getString(cursor.getColumnIndex("fl_sexo")));
        petModel.setDataNascimento(cursor.getString(cursor.getColumnIndex("dt_nascimento")));
        petModel.setEstadoCivil(cursor.getString(cursor.getColumnIndex("fl_estadoCivil")));
        petModel.setRegistroAtivo((byte)cursor.getShort(cursor.getColumnIndex("fl_ativo")));

        //RETORNANDO O PET
        return petModel;

    }

    /***
     * CONSULTA TODAS OS PETS CADASTRADOS NA BASE
     * @return
     */
    public List<PetModel> SelecionarTodos(){

        List<PetModel> pet = new ArrayList<PetModel>();


        //MONTA A QUERY A SER EXECUTADA
        StringBuilder stringBuilderQuery = new StringBuilder();
        stringBuilderQuery.append(" SELECT id_pet,      ");
        stringBuilderQuery.append("        ds_nome,        ");
        stringBuilderQuery.append("        ds_endereco,    ");
        stringBuilderQuery.append("        fl_sexo,        ");
        stringBuilderQuery.append("        dt_nascimento,  ");
        stringBuilderQuery.append("        fl_estadoCivil, ");
        stringBuilderQuery.append("        fl_ativo        ");
        stringBuilderQuery.append("  FROM  tb_pet       ");
        stringBuilderQuery.append(" ORDER BY ds_nome       ");


        //CONSULTANDO OS REGISTROS CADASTRADOS
        Cursor cursor = databaseUtil.GetConexaoDataBase().rawQuery(stringBuilderQuery.toString(), null);

        /*POSICIONA O CURSOR NO PRIMEIRO REGISTRO*/
        cursor.moveToFirst();


        PetModel petModel;

        //REALIZA A LEITURA DOS REGISTROS ENQUANTO NÃO FOR O FIM DO CURSOR
        while (!cursor.isAfterLast()){

            /* CRIANDO UM NOVO PET */
            petModel =  new PetModel();

            //ADICIONANDO OS DADOS DO PET
            petModel.setCodigo(cursor.getInt(cursor.getColumnIndex("id_pet")));
            petModel.setNome(cursor.getString(cursor.getColumnIndex("ds_nome")));
            petModel.setEndereco(cursor.getString(cursor.getColumnIndex("ds_endereco")));
            petModel.setSexo(cursor.getString(cursor.getColumnIndex("fl_sexo")));
            petModel.setDataNascimento(cursor.getString(cursor.getColumnIndex("dt_nascimento")));
            petModel.setEstadoCivil(cursor.getString(cursor.getColumnIndex("fl_estadoCivil")));
            petModel.setRegistroAtivo((byte)cursor.getShort(cursor.getColumnIndex("fl_ativo")));

            //ADICIONANDO UMA PET NA LISTA
            pet.add(petModel);

            //VAI PARA O PRÓXIMO REGISTRO
            cursor.moveToNext();
        }

        //RETORNANDO A LISTA DE PET
        return pet;

    }
}